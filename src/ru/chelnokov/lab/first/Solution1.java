package ru.chelnokov.lab.first;

import java.util.*;

/**
 * Класс, реализующий "первае задание"
 */
public class Solution1 {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(5,7,6,2,6,2,6743,5,7,3,5,6,6,2,63,7,5,564,4));
        ArrayList<Integer> unique = new ArrayList<>(toUnique(numbers));
        System.out.println(numbers.size());
        System.out.println(unique.size());
    }

    public static <T> Collection<T> toUnique(Collection<T> source) {
        return new HashSet<>(source);
    }
}
