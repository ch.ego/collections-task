package ru.chelnokov.lab.second;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Класс, реализующий "втарои задание"
 * Работа с ArrayList производится быстрее как минимум засчёт того,
 * что элементы LinkedList хранят в себе не только значения,
 * но и ссылки на предыдущий и следующий элементы
 */
public class Solution2 {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<>();

        run(arrayList);
        run(linkedList);
    }

    /**
     * вызывает необходимые методы для каждой из коллекций, замеряет время выполнения
     * @param collection коллекция целочисленных элементов
     */
    private static void run(Collection<Integer> collection) {
        long before = System.currentTimeMillis();
        addMillionNumbers(collection);
        rand100000Times(collection);
        long executionTime = System.currentTimeMillis() - before;
        System.out.println(executionTime);
    }

    /**
     * добавляет миллион целочисленных элементов к исходной коллекции
     * @param sourceCollection исходная коллекция целочисленных элементов
     */
    private static void addMillionNumbers(Collection<Integer> sourceCollection) {
        for (int i = 1; i <= 1_000_000; i++) {
            sourceCollection.add(i);
        }
    }

    /**
     * выбирает рандомный элемент коллекции сто тысяч раз
     * @param collection предварительно заполненная коллекция
     * @param <T> тип элементов в данном случае значения не имеет
     */
    private static <T> void rand100000Times(Collection<T> collection) {
        T random;
        for (int i = 0; i <= 100_000; i++) {
            random = collection.stream()
                    .skip((int) (collection.size() * Math.random()))
                    .findFirst().orElseThrow(NullPointerException::new);
        }
    }
}
